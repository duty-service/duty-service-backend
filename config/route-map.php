<?php
return [
    //ALGORITHM CONTROLLER
    ['class' => 'yii\rest\UrlRule',
        'controller' => ['v1/algorithm'],
        'pluralize' => false,
        'extraPatterns' => [
            'GET check' => 'check',
        ],
    ],
];