<?php

namespace app\modules\v1\controllers;

use app\models\repository\user\User;
use app\models\services\algorithm\AlgorithmCrudService;
use app\modules\v1\common\CommonController;
use Yii;

class AlgorithmController extends CommonController
{

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    public function actionCreate(): array
    {
        $algorithmCrudService = new AlgorithmCrudService();

        return $algorithmCrudService->add();
    }

    public function actionIndex()
    {
        return "It's alive!";
    }
}