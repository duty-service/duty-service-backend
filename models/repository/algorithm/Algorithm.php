<?php

namespace app\models\repository\algorithm;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "algorithm".
 *
 * @property int $id
 * @property string|null $title
 * @property int $created_at
 * @property int $updated_at
 *
 * @property AlgorithmItem[] $algorithmItems
 */
class Algorithm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'algorithm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    BaseActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    BaseActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ]
        ];
    }

    /**
     * Gets query for [[AlgorithmItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlgorithmItems()
    {
        return $this->hasMany(AlgorithmItem::className(), ['algorithm_id' => 'id'])->inverseOf('algorithm');
    }
}
