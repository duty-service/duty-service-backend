<?php

namespace app\models\services;

interface ICrudService
{
    public function add();

    public function update(int $id);

    public function delete(int $id);

}