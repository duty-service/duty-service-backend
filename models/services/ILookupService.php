<?php

namespace app\models\services;

interface ILookupService
{
    public function one($id);

    public function all();
}