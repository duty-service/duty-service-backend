<?php

namespace app\models\services\algorithm;

use app\models\repository\algorithm\Algorithm;
use app\models\services\ICrudService;
use Exception;
use Yii;

class AlgorithmCrudService implements ICrudService
{

    private array $request;

    public function __construct()
    {
        try {
            $this->request = Yii::$app->request->bodyParams;
        } catch (Exception $e) {
            Yii::warning($e);
        }
    }

    public function add(): array
    {
        $algorithm = new Algorithm();
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            if ($algorithm->load($this->request, '') && $algorithm->validate()) {
                $algorithm->save();
                $transaction->commit();
                return $algorithm->attributes;
            }
        } catch (\yii\base\Exception $e) {
            $transaction->rollBack();
            return $e->getTrace();
        } catch (Exception $e) {
            $transaction->rollBack();
            return $e->getTrace();
        }
        return [];
    }

    public function update(int $id)
    {
        // TODO: Implement update() method.
    }

    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }
}